﻿using System;
using System.Threading;
using RxEventBus;

namespace EventBusDemo
{
    #region Demo

    public class StringEvent : EventBase
    {
        public string Message => Id.ToString().Substring(0, 4);
    }

    public class StringEventConsumer : IEventHandler<StringEvent>
    {
        public void Receive(StringEvent message)
        {
            Console.WriteLine($"Received a message: {message?.Message}");
        }
    }

    public class HeavyConsumer : IEventHandler<StringEvent>
    {
        public void Receive(StringEvent message)
        {
            Console.WriteLine($"Heavy processing: {message?.Message}");
            Thread.Sleep(1000);
        }
    }
    #endregion
    class Program
    {
        static void Main(string[] args)
        {
            var eventBus = new EventBus();

            eventBus.Subscribe<StringEventConsumer, StringEvent>();
            eventBus.Subscribe<HeavyConsumer, StringEvent>();

            var disposable = eventBus.Scheduler.Schedule(TimeSpan.FromSeconds(1), () => new StringEvent());

            foreach (var subscriptionInfo in eventBus.GetSubscriptionInfo())
            {
                Console.WriteLine($"{subscriptionInfo.Message.Name} => {subscriptionInfo.Handler.Name}");
            }

            Console.ReadLine();
            eventBus.Unsubscribe<StringEventConsumer, StringEvent>();
            Console.WriteLine($"Unsubscribed for {nameof(StringEventConsumer)}");

            Console.ReadLine();
            disposable.Dispose();
            Console.WriteLine("Unscheduled");
        }
    }
}
