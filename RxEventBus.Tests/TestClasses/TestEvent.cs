﻿using System;

namespace RxEventBus.Tests.TestClasses
{
    public class TestEvent : EventBase, IEquatable<TestEvent>
    {
        public string Message { get; set; }
        
        public bool Equals(TestEvent other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return Id.Equals(other.Id) && string.Equals(Message, other.Message);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public static bool operator ==(TestEvent left, TestEvent right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(TestEvent left, TestEvent right)
        {
            return !Equals(left, right);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as TestEvent);
        }
    }
}