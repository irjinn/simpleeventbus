﻿
using System.Collections.Generic;
using System.Threading;

namespace RxEventBus.Tests.TestClasses
{
    public class TestEventHandler : IEventHandler<TestEvent>
    {
        private readonly object _lock = new object();
        public List<TestEvent> Events { get; } = new List<TestEvent>();

        public ManualResetEventSlim NotificationEvent { get; } = new ManualResetEventSlim();
        private readonly int _eventsToAwait;
        private int _eventFired;
        
        public TestEventHandler(int eventsToAwait)
        {
            _eventsToAwait = eventsToAwait;
        }

        public TestEventHandler() : this(1)
        {
            
        }

        public void Receive(TestEvent message)
        {
            lock (_lock)
            {
                if (++_eventFired == _eventsToAwait)
                {
                    NotificationEvent.Set();
                }
                Events.Add(message);
            }
        }
    }
}