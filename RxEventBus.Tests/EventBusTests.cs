﻿using System.Linq;
using System.Threading;
using NUnit.Framework;
using RxEventBus.Tests.TestClasses;

namespace RxEventBus.Tests
{
    [TestFixture]
    public class EventBusTests
    {
        public EventBus EventBus { get; set; } = new EventBus();

        [Test]
        public void EventBus_PublishEvent_NotifiesSubscriber()
        {
            //Arrange
            var subscriber = new TestEventHandler();
            EventBus.Subscribe<TestEventHandler, TestEvent>(subscriber);
            var expectedEvent = new TestEvent();
            //Act
            EventBus.Publish(expectedEvent);
            subscriber.NotificationEvent.Wait(500);

            //Assert
            CollectionAssert.AreEqual(subscriber.Events, new[] {expectedEvent});
        }

        [Test]
        public void EventBus_PublishEvent_NotifiesAllSubscribers()
        {
            //Arrange
            var subscribers = Enumerable.Range(0, 10).Select(_ => new TestEventHandler()).ToArray();
            foreach (var subscriber in subscribers)
            {
                EventBus.Subscribe<TestEventHandler, TestEvent>(subscriber);
            }

            var expectedEvent = new TestEvent();

            //Act
            EventBus.Publish(expectedEvent);
            WaitHandle.WaitAll(subscribers.Select(x => x.NotificationEvent.WaitHandle).ToArray(), 500);

            //Assert
            foreach (var subscriber in subscribers)
            {
                CollectionAssert.AreEqual(subscriber.Events, new[] { expectedEvent });
            }
        }

        [Test]
        public void EventBus_UnsubscribeInstance_StopsSendingNotifications()
        {
            //Arrange
            var subscriber = new TestEventHandler();
            EventBus.Subscribe<TestEventHandler, TestEvent>(subscriber);
            var expectedEvent = new TestEvent();

            //Act
            EventBus.Unsubscribe<TestEventHandler, TestEvent>(subscriber);
            EventBus.Publish(expectedEvent);

            var isNotificationFired = subscriber.NotificationEvent.Wait(50);

            //Assert
            Assert.False(isNotificationFired);
        }
        
        [Test]
        public void EventBus_UnsubscribeInstance_AnotherInstanceSubscriberKeepsReceiveNotifications()
        {
            //Arrange
            var attachedSubscriber = new TestEventHandler();
            var detachedSubscriber = new TestEventHandler();

            EventBus.Subscribe<TestEventHandler, TestEvent>(attachedSubscriber);
            EventBus.Subscribe<TestEventHandler, TestEvent>(detachedSubscriber);
            var expectedEvent = new TestEvent();

            //Act
            EventBus.Unsubscribe<TestEventHandler, TestEvent>(detachedSubscriber);
            EventBus.Publish(expectedEvent);

            var isNotificationFiredOnDetached = detachedSubscriber.NotificationEvent.Wait(100);
            var isNotificationFiredOnAttached = attachedSubscriber.NotificationEvent.Wait(100);

            //Assert
            Assert.True(isNotificationFiredOnAttached);
            Assert.False(isNotificationFiredOnDetached);
        }

        public class CountingHandler : IEventHandler<TestEvent>
        {
            public static void Reset()
            {
                TotalNotifications = 0;
                TotalInstancesCreated = 0;
            }
            
            public static int TotalNotifications;
            public static int TotalInstancesCreated;

            public CountingHandler()
            {
                Interlocked.Increment(ref TotalInstancesCreated);
            }

            public void Receive(TestEvent message)
            {
                Interlocked.Increment(ref TotalNotifications);
            }
        }

        [Test]
        public void EventBus_SubscribeNonInstance_CreatesHandlers()
        {
            //Arrange
            CountingHandler.Reset();
            EventBus.Subscribe<CountingHandler, TestEvent>();

            //Act
            EventBus.Publish(new TestEvent());
            EventBus.Publish(new TestEvent());
            
            Thread.Sleep(50); //fixme: find way to await gracefully

            //Assert
            Assert.AreEqual(2, CountingHandler.TotalNotifications);
            Assert.AreEqual(2, CountingHandler.TotalInstancesCreated);
        }

        [Test]
        public void EventBus_UnsubscribeNonInstance_StopsNotification()
        {
            //Arrange
            CountingHandler.Reset();
            EventBus.Subscribe<CountingHandler, TestEvent>();

            //Act
            EventBus.Publish(new TestEvent());
            EventBus.Publish(new TestEvent());

            EventBus.Unsubscribe<CountingHandler, TestEvent>();

            EventBus.Publish(new TestEvent());
            EventBus.Publish(new TestEvent());

            Thread.Sleep(50); //fixme: find way to await gracefully

            //Assert
            Assert.AreEqual(2, CountingHandler.TotalNotifications);
            Assert.AreEqual(2, CountingHandler.TotalInstancesCreated);
        }
        
        [Test]
        public void EventBus_MultipleNonInstanceSubscriptions_IgnoresAllExceptOne()
        {
            //Arrange
            CountingHandler.Reset();
            EventBus.Subscribe<CountingHandler, TestEvent>();
            EventBus.Subscribe<CountingHandler, TestEvent>();
            EventBus.Subscribe<CountingHandler, TestEvent>();

            //Act
            EventBus.Publish(new TestEvent());
            EventBus.Publish(new TestEvent());
            
            Thread.Sleep(50); //fixme: find way to await gracefully

            //Assert
            Assert.AreEqual(2, CountingHandler.TotalNotifications);
            Assert.AreEqual(2, CountingHandler.TotalInstancesCreated);
        }
        
        [Test]
        public void EventBus_UnsubscribeAfterMultipleNonInstanceSubscriptions_UnsubscribesCorrectly()
        {
            //Arrange
            CountingHandler.Reset();
            EventBus.Subscribe<CountingHandler, TestEvent>();
            EventBus.Subscribe<CountingHandler, TestEvent>();
            EventBus.Subscribe<CountingHandler, TestEvent>();

            //Act
            EventBus.Publish(new TestEvent());
            EventBus.Publish(new TestEvent());

            EventBus.Unsubscribe<CountingHandler, TestEvent>();

            EventBus.Publish(new TestEvent());
            EventBus.Publish(new TestEvent());

            Thread.Sleep(50); //fixme: find way to await gracefully

            //Assert
            Assert.AreEqual(2, CountingHandler.TotalNotifications);
            Assert.AreEqual(2, CountingHandler.TotalInstancesCreated);
        }
    }
}