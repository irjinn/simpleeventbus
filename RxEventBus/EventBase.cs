using System;

namespace RxEventBus
{
    public abstract class EventBase : IEvent
    {
        protected EventBase(Guid id)
        {
            Id = id;
        }

        protected EventBase() { }

        public virtual DateTimeOffset Created { get; } = DateTimeOffset.Now;
        public virtual Guid Id { get; } = Guid.NewGuid();
    }
}