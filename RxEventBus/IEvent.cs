using System;

namespace RxEventBus
{
    public interface IEvent
    {
        Guid Id { get; }
        DateTimeOffset Created { get; }
    }
}