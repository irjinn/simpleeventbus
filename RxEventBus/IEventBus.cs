namespace RxEventBus
{
    public interface IEventBus
    {
        void Subscribe<TEventHandler, TMessage>(TEventHandler handlerInstance) 
            where TMessage : IEvent 
            where TEventHandler : IEventHandler<TMessage>;

        void Subscribe<TEventHandler, TMessage>() 
            where TMessage : IEvent 
            where TEventHandler : IEventHandler<TMessage>, new();

        void Unsubscribe<TEventHandler, TMessage>() 
            where TMessage : IEvent
            where TEventHandler : IEventHandler<TMessage>, new();

        void Publish<TMessage>(TMessage message) 
            where TMessage : IEvent;
    }
}