namespace RxEventBus
{
    public interface IEventHandler<in TEvent> where TEvent : IEvent
    {
        void Receive(TEvent message);
    }
}