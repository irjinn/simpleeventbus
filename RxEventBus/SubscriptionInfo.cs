using System;

namespace RxEventBus
{
    public class SubscriptionInfo
    {
        public Type Message { get; set; }
        public Type Handler { get; set; }
    }
}