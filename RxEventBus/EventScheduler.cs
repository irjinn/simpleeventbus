using System;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

namespace RxEventBus
{
    public class EventScheduler : IEventScheduler
    {
        private readonly IEventBus _eventBus;

        public EventScheduler(IEventBus eventBus)
        {
            _eventBus = eventBus;
        }

        public IDisposable Schedule<TMessage>(TimeSpan interval, Func<TMessage> eventFactory) where TMessage : IEvent
        {
            return Observable.Interval(interval, TaskPoolScheduler.Default)
                .Select(_ => eventFactory())
                .Subscribe(msg => _eventBus.Publish(msg));
        }

        public IDisposable Schedule<TParam, TMessage>(TimeSpan interval, Func<TParam, TMessage> eventFactory, TParam param) where TMessage : IEvent
        {
            return Observable.Interval(interval, TaskPoolScheduler.Default)
                .Select(_ => eventFactory(param))
                .Subscribe(msg => _eventBus.Publish(msg));
        }
    }
}