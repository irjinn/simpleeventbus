﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Subjects;

namespace RxEventBus
{
    public class EventBus : IEventBus
    {
        private readonly ConcurrentDictionary<Type, object> _events = new ConcurrentDictionary<Type, object>();
        public IEventScheduler Scheduler { get; }

        public EventBus()
        {
            Scheduler = new EventScheduler(this);
        }

        public void Subscribe<TEventHandler, TMessage>() where TMessage : IEvent
            where TEventHandler : IEventHandler<TMessage>, new()
        {
            Subscribe(new EventBusSubscription<TMessage>(() => new TEventHandler())
            {
                SubscriberType = typeof (TEventHandler),
                SubscriberMode = SubscriberMode.DynamicallyCreated
            });
        }

        public void Subscribe<TEventHandler, TMessage>(TEventHandler handlerInstance) where TMessage : IEvent
            where TEventHandler : IEventHandler<TMessage>
        {
            Subscribe(new EventBusSubscription<TMessage>(() => handlerInstance)
            {
                SubscriberType = typeof (TEventHandler),
                SubscriberMode = SubscriberMode.Singleton
            });
        }

        private void Subscribe<TMessage>(EventBusSubscription<TMessage> subscription)
            where TMessage : IEvent
        {
            var eventInfo = GetEvent<TMessage>();

            if (eventInfo.AddSubscription(subscription))
            {
                subscription.Disposable = eventInfo.Subject
                    .ObserveOn(TaskPoolScheduler.Default)
                    .Subscribe(message => subscription.Subscriber.Receive(message));
            }
        }

        public void Publish<TEvent>(TEvent eventInstance) where TEvent : IEvent
        {
            object eventInfo;
            var type = typeof (TEvent);
            if (_events.TryGetValue(type, out eventInfo))
            {
                ((EventInfo<TEvent>) eventInfo).Subject.OnNext(eventInstance);
            }
        }

        public void Unsubscribe<TEventHandler, TMessage>() where TMessage : IEvent
            where TEventHandler : IEventHandler<TMessage>, new()
        {
            object eventInfo;
            if (_events.TryGetValue(typeof (TMessage), out eventInfo))
            {
                ((EventInfo<TMessage>) eventInfo).RemoveSubscription<TEventHandler>();
            }
        }

        public void Unsubscribe<TEventHandler, TMessage>(TEventHandler handlerInstance) where TMessage : IEvent
            where TEventHandler : IEventHandler<TMessage>, new()
        {
            object eventInfo;
            if (_events.TryGetValue(typeof (TMessage), out eventInfo))
            {
                ((EventInfo<TMessage>) eventInfo).RemoveSubscription(handlerInstance);
            }
        }

        public IEnumerable<SubscriptionInfo> GetSubscriptionInfo()
        {
            return _events
                .Select(x => (IEventInfo) x.Value)
                .SelectMany(
                    x => x.SubscribersTypes.Select(s => new SubscriptionInfo {Message = x.EventType, Handler = s}));
        }

        private EventInfo<TEvent> GetEvent<TEvent>() where TEvent : IEvent
        {
            var eventInfo = (EventInfo<TEvent>) _events.GetOrAdd(typeof (TEvent), t => new EventInfo<TEvent>());
            return eventInfo;
        }
    }

    #region System classes

    internal enum SubscriberMode
    {
        DynamicallyCreated,
        Singleton
    }

    internal interface IEventInfo
    {
        Type EventType { get; }
        IEnumerable<Type> SubscribersTypes { get; }
    }

    internal class EventInfo<TEvent> : IEventInfo where TEvent : IEvent
    {
        private readonly object _lock = new object();
        public ISubject<TEvent> Subject { get; } = new Subject<TEvent>();
        
        public Dictionary<Type, EventBusSubscription<TEvent>> DynamicSubscriptions { get; set; }
            = new Dictionary<Type, EventBusSubscription<TEvent>>();

        public Dictionary<IEventHandler<TEvent>, EventBusSubscription<TEvent>> InstanceSubscriptions { get; set; }
            = new Dictionary<IEventHandler<TEvent>, EventBusSubscription<TEvent>>();

        public Type EventType => typeof (TEvent);

        public IEnumerable<Type> SubscribersTypes
        {
            get
            {
                lock(_lock)
                {
                    return InstanceSubscriptions.Values.Select(x => x.SubscriberType)
                        .Concat(DynamicSubscriptions.Select(s => s.Value.SubscriberType))
                        .ToList();
                }
            }
        }

        /// <summary>
        /// Attempts to add subscription to internal collection. Returns false if such subscription already exists.
        /// </summary>
        /// <param name="subscription"></param>
        /// <returns></returns>
        public bool AddSubscription(EventBusSubscription<TEvent> subscription)
        {
            lock (_lock)
            {
                if (subscription.SubscriberMode == SubscriberMode.Singleton)
                {
                    var instance = subscription.Subscriber;
                    if (!InstanceSubscriptions.ContainsKey(instance))
                    {
                        InstanceSubscriptions[instance] = subscription;
                        return true;
                    }
                }
                else
                {
                    if (!DynamicSubscriptions.ContainsKey(subscription.SubscriberType))
                    {
                        DynamicSubscriptions[subscription.SubscriberType] = subscription;
                        return true;
                    }
                }
            }
            return false;
        }

        public void RemoveSubscription<TEventHandler>(TEventHandler handlerInstance)
            where TEventHandler : IEventHandler<TEvent>, new()
        {
            lock (_lock)
            {
                var subscription = InstanceSubscriptions.Values.FirstOrDefault(x => ReferenceEquals(handlerInstance, x.Subscriber));
                if (subscription != null)
                {
                    InstanceSubscriptions.Remove(handlerInstance);
                    subscription.Disposable.Dispose();
                }
            }
        }

        public void RemoveSubscription<TEventHandler>()
            where TEventHandler : IEventHandler<TEvent>, new()
        {
            lock (_lock)
            {
                EventBusSubscription<TEvent> subscription;
                var subscriberType = typeof (TEventHandler);
                if (DynamicSubscriptions.TryGetValue(subscriberType, out subscription))
                {
                    DynamicSubscriptions.Remove(subscriberType);
                    subscription.Disposable.Dispose();
                }
            }
        }


    }

    internal class EventBusSubscription<TMessage> : IDisposable where TMessage : IEvent
    {
        public virtual IEventHandler<TMessage> Subscriber => _handlerFactory();

        public IDisposable Disposable { get; set; }
        private Func<IEventHandler<TMessage>> _handlerFactory;

        public Type SubscriberType { get; set; }
        public SubscriberMode SubscriberMode { get; set; }

        public EventBusSubscription(Func<IEventHandler<TMessage>> handlerFactory)
        {
            _handlerFactory = handlerFactory;
        }

        public void Dispose()
        {
            Disposable?.Dispose();
            _handlerFactory = null;
        }
    }

    #endregion
}