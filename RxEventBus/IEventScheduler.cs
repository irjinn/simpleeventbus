using System;

namespace RxEventBus
{
    public interface IEventScheduler
    {
        IDisposable Schedule<TMessage>(TimeSpan interval, Func<TMessage> eventFactory) where TMessage : IEvent;
        IDisposable Schedule<TParam, TMessage>(TimeSpan interval, Func<TParam, TMessage> eventFactory, TParam param) where TMessage : IEvent;
    }
}